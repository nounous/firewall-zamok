#!/bin/env python3
import datetime
import ipaddress
import json
import os
import subprocess
import sys

import jinja2
import ldap

path = os.path.dirname(os.path.abspath(__file__))

def trim_query(query):
    return [
        (dn, {key : [ value.decode('utf-8') for value in values ] for key,values in entry.items() })
            for dn, entry in query
    ]

def expand_description(query):
    for _,entry in query:
        if 'description' not in entry:
            continue
        desc = entry['description']
        entry.pop('description', None)
        for attr in desc:
            k,v = attr.split(':',1)
            if k not in entry:
                entry[k] = []
            entry[k].append(v)

if __name__ == "__main__":
    with open(os.path.join(path, "firewall-zamok.json")) as config_file:
        config = json.load(config_file)

    base = ldap.initialize(config['ldap']['url'])
    base.simple_bind_s(config['ldap']['binddn'], config['ldap']['password'])

    users_query_id = base.search(config["ldap"]["base"], ldap.SCOPE_ONELEVEL, "objectClass=posixAccount")
    users = base.result(users_query_id)
    users = [ user["uidNumber"][0].decode('utf-8') for _, user in users[1] ]
    users = [ user for user in users ]

    restricted_interfaces = config["restricted_interfaces"]

    base_adh = ldap.initialize(config['ldap_adh']['server'])
    if config['ldap_adh']['server'].startswith('ldaps://'):
        base_adh.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        base_adh.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
    base_adh.simple_bind_s(config['ldap_adh']['binddn'], config['ldap_adh']['bindpass'])

    users_query_id = base_adh.search(config['ldap_adh']['userdn'], ldap.SCOPE_ONELEVEL, '(&(objectClass=inetOrgPerson)(description=membershipEnd:*))')
    users_query = base_adh.result(users_query_id)[1]
    users_query = trim_query(users_query)
    expand_description(users_query)
    members = { dn: entry['cn'][0] for dn, entry in users_query
        if 'membershipEnd' in entry and
            datetime.date.fromisoformat(entry['membershipEnd'][0]) >= datetime.date.today()
    }
    member_uids = {
        entry['cn'][0]: int(entry['uidNumber'][0]) for dn, entry in users_query
            if 'uidNumber' in entry
    }

    clubs_query_id = base_adh.search(config['ldap_adh']['clubdn'], ldap.SCOPE_ONELEVEL, '(&(objectClass=organization)(description=*))')
    clubs_query = base_adh.result(clubs_query_id)[1]
    clubs_query = trim_query(clubs_query)
    clubs = {
        dn: entry['description'] for dn, entry in clubs_query if 'description' in entry
    }

    hosts_query_id = base_adh.search(config['ldap_adh']['hostdn'], ldap.SCOPE_ONELEVEL)
    hosts_query = base_adh.result(hosts_query_id)[1]
    hosts_query = trim_query(hosts_query)

    hosts_adh = []
    for dn, obj in hosts_query:
        owners = []
        for owner in obj['owner']:
            if owner in members:
                owners.append(member_uids[members[owner]])
            elif owner in clubs:
                for club_owner in clubs[owner]:
                    owners.append(member_uids[club_owner])

        ip_addresses = []
        for ip in obj['ipHostNumber']:
            ip = ipaddress.ip_address(ip)
            if any(ip in ipaddress.ip_network(network) for network in config['networks_adh']):
                ip_addresses.append(ip)

        if ip_addresses and owners:
            hosts_adh.append({'name': obj['cn'][0], 'ip_addresses': ip_addresses, 'owners': owners})

    with open(os.path.join(path, 'templates', 'nftables.conf.j2')) as firewall_template:
        template = jinja2.Template(firewall_template.read())

    with open('/etc/nftables.conf', 'w') as nftables:
        nftables.write(template.render(users=users, restricted_interfaces=restricted_interfaces, hosts_adh=hosts_adh))
    subprocess.run(['systemctl', 'reload', 'nftables'])
